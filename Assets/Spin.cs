﻿using UnityEngine;
public class Spin : MonoBehaviour
{
    public void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * 100, Space.World);
    }

    public void Pause()
    {
        this.enabled = !this.enabled;
    }

}
