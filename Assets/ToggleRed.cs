﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleRed : MonoBehaviour
{
	public void toggleRed() {

        Material currentMaterial = GetComponent<Renderer>().material;
        if (currentMaterial.color != Color.red)
            currentMaterial.color = Color.red;
        else
            currentMaterial.color = Color.white;
    }
}
